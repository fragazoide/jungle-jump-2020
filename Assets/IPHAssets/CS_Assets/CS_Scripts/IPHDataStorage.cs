﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using InfiniteHopper.Types;
using System.Collections.Generic;
using System;

namespace InfiniteHopper
{
    /// <summary>
    /// Class that control the storage data
    /// </summary>
    public class IPHDataStorage : MonoBehaviour
    {
        /// <summary>
        /// Reference to singleton
        /// </summary>
        public static IPHDataStorage instance;
        /// <summary>
        /// The persistent player data
        /// </summary>
        public static DataCloud playerData;

        /// <summary>
        /// Create the singleton
        /// </summary>
        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
                return;
            }
            instance = this;
            DontDestroyOnLoad(gameObject);

            playerData = LoadData();
        }

        private void OnApplicationFocus(bool focus)
        {
            SaveData(playerData);
        }

        private void OnApplicationQuit()
        {
            SaveData(playerData);
        }

        // Called when destroy the object
        private void OnDestroy()
        {
            SaveData(playerData);    
        }

        /// <summary>
        /// Save the player data to json file
        /// </summary>
        /// <param name="data">Reference data to save</param>
        public void SaveData(DataCloud data)
        {
            string json = JsonUtility.ToJson(data);

            StreamWriter tw = new StreamWriter(Application.persistentDataPath + "/data.sv");
            tw.Write(json);
            tw.Close();
            Debug.Log("Saving as JSON: " + json);
        }

        /// <summary>
        /// Load data from json
        /// </summary>
        /// <param name="json">Json to load data</param>
        /// <returns>Return instance of DataCloud</returns>
        public DataCloud LoadData()
        {
            if (!File.Exists(Application.persistentDataPath + "/data.sv"))
                return new DataCloud();

            TextReader tr = new StreamReader(Application.persistentDataPath + "/data.sv");
            string json = tr.ReadToEnd();

            Debug.Log("Load as JSON: " + json);

            DataCloud data = JsonUtility.FromJson<DataCloud>(json);
            return data?? new DataCloud();
        }

        /// <summary>
        /// Delete all data save and reset profile
        /// </summary>
        public void ResetData()
        {
            if (File.Exists(Application.dataPath + "/data.sv"))
                File.Delete(Application.dataPath + "/data.sv");

            PlayerPrefs.DeleteAll();

            playerData = new DataCloud();
        }
    }
}
