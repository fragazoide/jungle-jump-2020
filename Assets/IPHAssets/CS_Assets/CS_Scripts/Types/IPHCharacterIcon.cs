﻿using UnityEngine;
using System;

namespace InfiniteHopper
{
    [Serializable]
    public class IPHCharacterIcon
    {
        [SerializeField] private GameObject _characterObject = default;

        public int TokensToUnlock { get; set; }
        
        public bool Unlocked { get; set; }

        public void SetCharacterActive(bool active)
        {
            _characterObject.SetActive(active);
        }

        public SpriteRenderer[] GetAllSpriteRenderers()
        {
            return _characterObject.GetComponentsInChildren<SpriteRenderer>();
        }
    }
}


