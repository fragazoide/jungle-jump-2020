﻿using System;
using System.Collections.Generic;
using OktagonGDK.Achievements;

namespace InfiniteHopper.Types
{
    [Serializable]
    public class DataCloud
    {
        //Last score
        public int lastScore;
        //Current highscore
        public int highScore;
        //Total amount of score
        public int totalScore;
        //Total of feathers
        public int tokens;
        //Number of characters unlocked
        public int charactersUnlocked;
        //Current longest distance
        public int longestDistance;
        //Current longest streak
        public int longestStreak;
        //Total of powerups
        public int totalPowerups;
        //Current longest powerup
        public int longestPowerUpStreak;
        //Total of game played
        public int rounds;
        //SFX On or Off
        public bool sfxIsToggled = true;
        //Music On or Off
        public bool musicIsToggled = true;
        //Time for reward to be claimed again
        public long rewardClaimTimer;
        //
        public List<AchievementProgression> achievementsProgression;

        /// <summary>
        /// Set the score and update the highscore variable when score is greater
        /// </summary>
        public void SetNewScore(int score)
        {
            lastScore = score;
            highScore = score > highScore ? score : highScore;
            totalScore += score;
        }
    }
}
