﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IPHAchievementTrigger : AchievementTrigger
{
    public IPHAchievementTrigger() { }

    public IPHAchievementTrigger(int value, string name) : base(value, name) { }

    public static readonly AchievementTrigger Jump_Platform = new AchievementTrigger(0, "Jump_Platform");
    public static readonly AchievementTrigger Collect_Powerup = new AchievementTrigger(1, "Collect_Powerup");
    public static readonly AchievementTrigger Unlock_Character = new AchievementTrigger(2, "Unlock_Character");
    public static readonly AchievementTrigger Play_Matches = new AchievementTrigger(3, "Play_Matches");
}
