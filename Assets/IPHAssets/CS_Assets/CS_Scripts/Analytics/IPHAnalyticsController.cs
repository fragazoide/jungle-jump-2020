﻿using mixpanel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteHopper
{
    public class IPHAnalyticsController : SingletonBehaviour<IPHAnalyticsController>
    {
        protected override void OnSingletonAwake()
        {
            DontDestroyOnLoad(this);
            AnalyticsService.Initialize();
        }

        /// <summary>
        /// This method should be used instead of MonoBehaviour.OnDestroy()
        /// </summary>
        protected override void OnSingletonDestroy()
        {
            AnalyticsService.LogEvent("OnDestroy");
        }

        #region Public Methods
        public void TrackGameStarted(string characterName)
        {
            var pProperties = new Value();
            pProperties["CharacterName"] = characterName;

            TrackEvent("GameStarted", pProperties);
            LogEvent("GameStarted", pProperties);
        }

        public void TrackGameEnd(string characterName, int currentScore)
        {
            var pProperties = new Value();
            pProperties["CharacterName"] = characterName;
            pProperties["CurrentScore"] = currentScore;

            TrackEvent("GameEnded", pProperties);
            LogEvent("GameEnded", pProperties);
        }

        public void TrackPowerUp(string sPowerUpName)
        {
            var pProperties = new Value();
            pProperties["PowerUp"] = sPowerUpName;

            TrackEvent("PowerUp_Collected", pProperties);
            LogEvent("PowerUp_Collected", pProperties);
        }

        public void TrackToggleSound(string sSoundConfig, bool bStatus)
        {
            var pProperties = new Value();
            pProperties["Sound_Config"] = sSoundConfig;
            pProperties["Status"] = bStatus;

            Mixpanel.People.Set(sSoundConfig + "Pref", bStatus);
            TrackEvent("SoundConfigChanged", pProperties);
            LogEvent("SoundConfigChanged", pProperties);
        }

        #endregion

        #region Private Methods

        private void TrackEvent(string eventName, Value properties)
        {
            properties["Session"] = IPHDataStorage.playerData.rounds;
            properties["Highscore"] = IPHDataStorage.playerData.highScore;
            properties["UnlockedCharacters"] = IPHDataStorage.playerData.charactersUnlocked;
            AnalyticsService.TrackEvent(eventName, properties);
        }

        private void LogEvent(string eventName, Value properties)
        {
            AnalyticsService.LogEvent(eventName, properties);
        }

        #endregion
    }
}