﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OktagonGDK.UI;

namespace InfiniteHopper.Audio
{
    public class IPHAudioConfiguration : MonoBehaviour
    {
        [SerializeField] private BaseToggleUI _soundToggle = default;
        [SerializeField] private BaseToggleUI _musicToggle = default;

        private void Start()
        {
            _soundToggle.onValueChanged.AddListener(OnSoundToggle);
            _musicToggle.onValueChanged.AddListener(OnMusicToggle);

            UpdateSoundToggleState(IPHDataStorage.playerData.sfxIsToggled);
            UpdateMusicToggleState(IPHDataStorage.playerData.musicIsToggled);
        }

        private void UpdateSoundToggleState(bool state)
        {
            _soundToggle.SetState(state);
            IPHAudioController.Instance.SetSFXToggled(state);
        }

        private void UpdateMusicToggleState(bool state)
        {
            _musicToggle.SetState(state);
            IPHAudioController.Instance.SetMusicToggled(state);
        }

        private void OnSoundToggle(bool toggled)
        {
            IPHDataStorage.playerData.sfxIsToggled = toggled;
            IPHAnalyticsController.Instance.TrackToggleSound("SFX_Option", toggled);
            IPHAudioController.Instance.SetSFXToggled(toggled);
        }

        private void OnMusicToggle(bool toggled)
        {
            IPHDataStorage.playerData.musicIsToggled = toggled;
            IPHAnalyticsController.Instance.TrackToggleSound("Music_Option", toggled);
            IPHAudioController.Instance.SetMusicToggled(toggled);
        }
    }
}
