﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteHopper.Audio
{
    public class IPHAudioController : SingletonBehaviour<IPHAudioController>
    {
        [Header("Audio Controller")]
        [SerializeField] private AudioSource _musicSource = default;
        [SerializeField] private AudioSource _sfxSource = default;

        private bool _musicIsToggled;
        private bool _sfxIsToggled;

        #region SFX Functions

        public void PlaySFX(AudioClip clip)
        {
            if (clip != null && _sfxIsToggled)
                _sfxSource.PlayOneShot(clip);
        }

        public void StopSFX()
        {
            _sfxSource.Stop();
        }

        public void SetSFXToggled(bool toggled)
        {
            _sfxIsToggled = toggled;
            _sfxSource.enabled = toggled;
        }

        public void SetSFXVolume(float volume)
        {
            _sfxSource.volume = volume;
        }

        public void SetSFXPitch(float pitch)
        {
            _sfxSource.pitch = pitch;
        }

        #endregion

        #region Music Functions
        public void PlayMusic(AudioClip clip, bool loop = true)
        {
            if (clip != null)
            {
                _musicSource.clip = clip;
                _musicSource.loop = loop;
            }

            if (_musicIsToggled && _musicSource.clip != null)
                _musicSource.Play();
        }

        public void StopMusic()
        {
            _musicSource.Stop();
        }

        public void SetMusicToggled(bool toggled)
        {
            _musicIsToggled = toggled;
            _musicSource.enabled = toggled;
        }
        #endregion
    }
}