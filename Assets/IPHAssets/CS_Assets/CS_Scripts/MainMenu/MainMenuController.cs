﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : SingletonBehaviour<MainMenuController>
{
    [Header("Buttons")]
    [SerializeField] private Button _achievementsButton = default;
    [SerializeField] private Button _statsButton = default;
    [SerializeField] private Button _highscoreButton = default;
    [Header("Canvas Objects")]
    [SerializeField] private GameObject _statsCanvas = default;
    [SerializeField] private GameObject _highscoreCanvas = default;

    public Action OnAchievementsButtonClicked;

    private void Start()
    {
        _achievementsButton.onClick.AddListener(() => OnAchievementsButtonClicked?.Invoke());
        _statsButton.onClick.AddListener(() => _statsCanvas.SetActive(true));
        _highscoreButton.onClick.AddListener(() => _highscoreCanvas.SetActive(true));
    }
}
