﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InfiniteHopper.Audio;
using OktagonGDK.Achievements;

namespace InfiniteHopper
{
    /// <summary>
    /// This script defines the player, its movement, as well as its death
    /// </summary>
    public class IPHPlayer : MonoBehaviour
    {
        internal List<float> jumpHistory = new List<float>();
        internal Transform thisTransform;
        internal GameObject gameController;

        //The current jump power of the player
        internal float jumpPower = 2;

        //Should the player automatically jump when reaching the maximum power?
        internal bool autoJumpWhenMaxed = true;

        //Minimun jump power when the player gives a quick tap on screen
        internal float minimumJumpPower = 5f;

        //A power bar that shows how high the player will jump.
        internal Transform powerBar;

        //The particle effects that will play when the player jumps and lands on a column
        internal ParticleSystem jumpEffect;
        internal ParticleSystem landEffect;
        internal ParticleSystem perfectEffect;

        //The main sprite renderer to change the visual of player
        public BoxCollider2D boxCcollider;
        public string powerbarTag = "PowerBar";

        //Did the player start the jump process ( powering up and then releasing )
        internal bool startedJump = false;

        //Can the player use the Double Jump feature
        internal bool canDoubleJump = false;

        //Can the player use the Dash feature
        internal bool canDash = true;

        //Has the player used the power move
        internal bool usedPowerMove = false;

        //Is the player jumping now ( is in mid-air )
        internal bool isJumping = false;

        //Has the player landed on a column?
        internal bool isLanded = false;

        // Is the player falling now?
        internal bool isFalling = false;

        // Is the player dead?
        internal bool isDead = false;

        /// <summary>
        /// Store the player config
        /// </summary>
        public IPHPlayerScriptable PlayerPresets { get; set; }

        #region Private Methods

        private void Start()
        {
            thisTransform = transform;

            //Assign the game controller for easier access
            gameController = GameObject.FindGameObjectWithTag("GameController");

            //Create the effects
            jumpEffect = (Instantiate(PlayerPresets.JumpEffect.gameObject) as GameObject).GetComponent<ParticleSystem>();
            landEffect = (Instantiate(PlayerPresets.LandEffect.gameObject) as GameObject).GetComponent<ParticleSystem>();
            perfectEffect = (Instantiate(PlayerPresets.PerfectEffect.gameObject) as GameObject).GetComponent<ParticleSystem>();

            //Set the parent to the effects
            jumpEffect.transform.SetParent(transform, false);
            landEffect.transform.SetParent(transform, false);
            perfectEffect.transform.SetParent(transform, false);

            //Set the particle effects to the "RenderInFront" sorting layer so that the effects appear in front of the player object
            if (jumpEffect) jumpEffect.GetComponent<Renderer>().sortingLayerName = "RenderInFront";
            if (landEffect) landEffect.GetComponent<Renderer>().sortingLayerName = "RenderInFront";
            if (perfectEffect) perfectEffect.GetComponent<Renderer>().sortingLayerName = "RenderInFront";

            canDoubleJump = PlayerPresets.CanDoubleJump;
            canDash = PlayerPresets.CanDash;

            //Change the player visual
            SetupAvatar();

            //Assign the powerbar
            if (GameObject.FindGameObjectWithTag(powerbarTag)) powerBar = GameObject.FindGameObjectWithTag(powerbarTag).transform;            
        }

        private void Update()
        {
            if (isDead == false)
            {
                //If we are starting to jump, charge up the jump power as long as we are holding the jump button down
                if (startedJump == true)
                {
                    //Charge up the jump power
                    if (jumpPower < PlayerPresets.JumpChargeMax)
                    {
                        //Add to the jump power based on charge speed
                        jumpPower += Time.deltaTime * PlayerPresets.JumpChargeSpeed;

                        if (powerBar)
                        {
                            //Align the power bar to the player
                            powerBar.position = thisTransform.position;

                            //Update the power bar fill amount
                            powerBar.Find("Base/FillAmount").GetComponent<Image>().fillAmount = jumpPower / PlayerPresets.JumpChargeMax;
                        }

                        //Play the charge sound and change the pitch based on the jump power
                        IPHAudioController.Instance.SetSFXPitch(0.3f + jumpPower * 0.1f);
                    }
                    else if (autoJumpWhenMaxed == true)
                    {
                        //If the jump power exceeds the maximum allowed jump power, end charging, and launch the player
                        EndJump();
                    }
                    else
                    {
                        //Play the full power animation
                        if (GetComponent<Animation>() && PlayerPresets.AnimationFullPower) GetComponent<Animation>().Play(PlayerPresets.AnimationFullPower.name);
                    }
                }

                //If the player is moving down, then he is falling
                if (isFalling == false && GetComponent<Rigidbody2D>().velocity.y < 0)
                {
                    isFalling = true;

                    //Play the falling animation
                    if (GetComponent<Animation>() && PlayerPresets.AnimationFalling)
                    {
                        //Play the animation
                        GetComponent<Animation>().PlayQueued(PlayerPresets.AnimationFalling.name, QueueMode.CompleteOthers);
                    }
                }
            }
        }

        //Setup the player visual
        private void SetupAvatar()
        {
            //Create the visual avatar of player
            GameObject avatar = Instantiate(PlayerPresets.Avatar) as GameObject;
            avatar.name = "Part1";
            avatar.transform.SetParent(thisTransform.GetChild(0), false);

            //Setup the collider that close the visual of player
            boxCcollider.offset = PlayerPresets.ColliderOffset;
            boxCcollider.size = PlayerPresets.ColliderSize;
        }
        #endregion

        #region Public Methods

        //This function adds score to the gamecontroller
        public void ChangeScore(Transform landedObject)
        {
            gameController.SendMessage("ChangeScore", landedObject);
        }

        //This function destroys the player, and triggers the game over event
        public void Died()
        {
            if (isDead == false)
            {
                //Call the game over function from the game controller
                gameController.SendMessage("GameOver", 0.5f);

                IPHAudioController.Instance.SetSFXPitch(1);
                IPHAudioController.Instance.PlaySFX(PlayerPresets.SoundCrash);

                // The player is dead
                isDead = true;
            }
        }

        // This function resets the player's dead status
        public void ResetDead()
        {
            isDead = false;
        }

        //This function starts the jumping process, allowing the player to charge up the jump power as long as he is holding the jump button down
        public void StartJump(bool playerAutoJump)
        {
            if (isDead)
                return;

            //Set the player auto jump state based on the GameController playerAutoJump value
            autoJumpWhenMaxed = playerAutoJump;

            //You can only jump if you are on land
            if (isLanded)
            {
                startedJump = true;
                jumpPower = 0;
                ChargeJump();
            }

            if (isJumping && !usedPowerMove)
                UsePowerMove();
        }

        //This function ends the jump process, and launches the player with the jump power we charged
        public void EndJump()
        {
            if (isDead)
                return;

            //You can only jump if you are on land, and you already charged up the jump power ( jump start )
            if (startedJump)
            {
                thisTransform.parent = null;
                startedJump = false;
                isJumping = true;
                isLanded = false;
                isFalling = false;
                CompleteJump();
            }
        }

        // Charges the jump when called
        private void ChargeJump()
        {
            //Play the jump start animation ( charging up the jump power )
            if (GetComponent<Animation>() && PlayerPresets.AnimationJumpStart)
            {
                //Stop the animation
                GetComponent<Animation>().Stop();

                //Play the animation
                GetComponent<Animation>().Play(PlayerPresets.AnimationJumpStart.name);
            }
            
            powerBar.gameObject.SetActive(true);
            IPHAudioController.Instance.PlaySFX(PlayerPresets.SoundStartJump);
        }

        // Completes the jump charge and releases the player
        private void CompleteJump()
        {
            //Sets the jump power to the minimum force if is lower
            jumpPower = jumpPower < minimumJumpPower ? minimumJumpPower : jumpPower;

            //Give the player velocity based on jump power and move speed
            GetComponent<Rigidbody2D>().velocity = new Vector2(PlayerPresets.MoveSpeed, jumpPower);

            //Play the jump ( launch ) animation
            if (GetComponent<Animation>() && PlayerPresets.AnimationJumpEnd)
            {
                //Stop the animation
                GetComponent<Animation>().Stop();

                //Play the animation
                GetComponent<Animation>().Play(PlayerPresets.AnimationJumpEnd.name);
            }

            //Deactivate the power bar
            if (powerBar) powerBar.gameObject.SetActive(false);

            //Play the jump particle effect
            if (jumpEffect) jumpEffect.Play();

            //Play the jump sound ( launch )
            IPHAudioController.Instance.StopSFX();
            IPHAudioController.Instance.SetSFXPitch(0.6f + jumpPower * 0.05f);
            IPHAudioController.Instance.PlaySFX(PlayerPresets.SoundEndJump);
        }

        // Activates one of the two powers
        public void UsePowerMove()
        {
            usedPowerMove = true;

            if (canDoubleJump)
            {
                startedJump = true;
                jumpPower = 0;
                ChargeJump();
            }

            if (canDash)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(PlayerPresets.MoveSpeed * 2f, 0);
            }
        }

        //This function runs when the player succesfully lands on a column
        public void PlayerLanded()
        {
            isLanded = true;
            isJumping = false;
            usedPowerMove = false;

            //Play the landing animation
            if (GetComponent<Animation>() && PlayerPresets.AnimationLanded)
            {
                //Stop the animation
                GetComponent<Animation>().Stop();

                //Play the animation
                GetComponent<Animation>().Play(PlayerPresets.AnimationLanded.name);
            }

            //Play the landing particle effect
            if (landEffect) landEffect.Play();

            //Play the landing sound
            IPHAudioController.Instance.SetSFXPitch(1);
            IPHAudioController.Instance.PlaySFX(PlayerPresets.SoundLand);
            AchievementController.Instance.TriggerAchievement(IPHAchievementTrigger.Jump_Platform);
        }

        //This function runs when the player executes a perfect landing ( closest to the middle )
        public void PerfectLanding(int streak)
        {
            //Play the perfect landing particle effect
            if (perfectEffect) perfectEffect.Play();

            //If there is a sound source and a sound assigned, play it from the source
            IPHAudioController.Instance.PlaySFX(PlayerPresets.SoundPerfect);
        }

        //This function rescales this object over time
        public void Rescale(float targetScale)
        {
            StartCoroutine(RescaleRoutine());

            IEnumerator RescaleRoutine()
            {
                //Perform the scaling action for 1 second
                float scaleTime = 1;

                while (scaleTime > 0)
                {
                    //Count down the scaling time
                    scaleTime -= Time.deltaTime;

                    //Wait for the fixed update so we can animate the scaling
                    yield return new WaitForFixedUpdate();

                    float tempScale = thisTransform.localScale.x;

                    //Scale the object up or down until we reach the target scale
                    tempScale -= (thisTransform.localScale.x - targetScale) * 5 * Time.deltaTime;

                    thisTransform.localScale = Vector3.one * tempScale;
                }

                //Rescale the object to the target scale instantly, so we make sure that we got the the target
                thisTransform.localScale = Vector3.one * targetScale;
            }
        }
        #endregion
    }
}