﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OktagonGDK.Achievements;

namespace InfiniteHopper
{
    public static class IPHPlayerUnlock
    {
        public const int BunnyUnlockRequirement = 1;
        public const int DoggyUnlockRequirement = 10;
        public const int PiggyUnlockRequirement = 15;
        public const int PeggyUnlockRequirement = 20;
        public const int PandaUnlockRequirement = 25;

        public static bool BunnyUnlocked;
        public static bool DoggyUnlocked;
        public static bool PiggyUnlocked;
        public static bool PeggyUnlocked;
        public static bool PandaUnlocked;

        public static void UnlockCharacters()
        {
            int tokens = IPHDataStorage.playerData.tokens;
            BunnyUnlocked = tokens >= BunnyUnlockRequirement;
            DoggyUnlocked = tokens >= DoggyUnlockRequirement;
            PiggyUnlocked = tokens >= PiggyUnlockRequirement;
            PeggyUnlocked = tokens >= PeggyUnlockRequirement;
            PandaUnlocked = tokens >= PandaUnlockRequirement;
            
            UnlockAchievements();
        }

        private static void UnlockAchievements()
        {
            if (BunnyUnlocked) AchievementController.Instance.TriggerAchievement(IPHAchievementTrigger.Unlock_Character, "Bunny");
            if (DoggyUnlocked) AchievementController.Instance.TriggerAchievement(IPHAchievementTrigger.Unlock_Character, "Doggy");
            if (PiggyUnlocked) AchievementController.Instance.TriggerAchievement(IPHAchievementTrigger.Unlock_Character, "Piggy");
            if (PeggyUnlocked) AchievementController.Instance.TriggerAchievement(IPHAchievementTrigger.Unlock_Character, "Peggy");
            if (PandaUnlocked) AchievementController.Instance.TriggerAchievement(IPHAchievementTrigger.Unlock_Character, "Panda");
        }
    }
}