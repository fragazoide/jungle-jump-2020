﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using InfiniteHopper.Types;
using System.Collections.Generic;

namespace InfiniteHopper
{
    /// <summary>
    ///This script handles player selection. You can navigate through the available players using buttons. 
    /// </summary>
    public class IPHPlayerSelection : MonoBehaviour
    {
        [Header("Characters")]
        [SerializeField] private IPHCharacterIcon _monkeyCharacter = default;
        [SerializeField] private IPHCharacterIcon _bunnyCharacter = default;
        [SerializeField] private IPHCharacterIcon _doggyCharacter = default;
        [SerializeField] private IPHCharacterIcon _piggyCharacter = default;
        [SerializeField] private IPHCharacterIcon _peggyCharacter = default;
        [SerializeField] private IPHCharacterIcon _pandaCharacter = default;

        [Header("Tokens")]
        [SerializeField] private GameObject _tokenObject = default;
        [SerializeField] private Text _tokenText = default;

        private List<IPHCharacterIcon> _charactersList = new List<IPHCharacterIcon>();
        private int _currentCharacter = 0;
        private int _index = 0;

        void Start()
        {
            UnlockCharacters();
            ComputeCharactersUnlocked();
            SetPlayer(_currentCharacter);
        }

        //This function changes the current player
        public void ChangePlayer(int changeValue)
        {
            _currentCharacter += changeValue;

            if (_currentCharacter > _charactersList.Count - 1)
                _currentCharacter = 0;
            
            if (_currentCharacter < 0)
                _currentCharacter = _charactersList.Count - 1;

            SetPlayer(_currentCharacter);
        }

        //Unlock the characters
        private void UnlockCharacters()
        {
            IPHPlayerUnlock.UnlockCharacters();

            _monkeyCharacter.Unlocked = true;
            _bunnyCharacter.Unlocked = IPHPlayerUnlock.BunnyUnlocked;
            _doggyCharacter.Unlocked = IPHPlayerUnlock.DoggyUnlocked;
            _peggyCharacter.Unlocked = IPHPlayerUnlock.PeggyUnlocked;
            _piggyCharacter.Unlocked = IPHPlayerUnlock.PiggyUnlocked;
            _pandaCharacter.Unlocked = IPHPlayerUnlock.PandaUnlocked;

            _monkeyCharacter.TokensToUnlock = 0;
            _bunnyCharacter.TokensToUnlock = IPHPlayerUnlock.BunnyUnlockRequirement;
            _doggyCharacter.TokensToUnlock = IPHPlayerUnlock.DoggyUnlockRequirement;
            _peggyCharacter.TokensToUnlock = IPHPlayerUnlock.PeggyUnlockRequirement;
            _piggyCharacter.TokensToUnlock = IPHPlayerUnlock.PiggyUnlockRequirement;
            _pandaCharacter.TokensToUnlock = IPHPlayerUnlock.PandaUnlockRequirement;

            _charactersList.Add(_monkeyCharacter);
            _charactersList.Add(_bunnyCharacter);
            _charactersList.Add(_doggyCharacter);
            _charactersList.Add(_peggyCharacter);
            _charactersList.Add(_piggyCharacter);
            _charactersList.Add(_pandaCharacter);
        }

        //Check the number of characters unlocked
        private void ComputeCharactersUnlocked()
        {
            int charactersUnlocked = 0;
            
            foreach (var character in _charactersList)
                charactersUnlocked += character.Unlocked ? 1 : 0;

            IPHDataStorage.playerData.charactersUnlocked = charactersUnlocked;
        }

        //This function activates the selected player, while deactivating all the others
        private void SetPlayer(int playerNumber)
        {
            //Go through all the players, and hide each one except the current player
            for (_index = 0; _index < _charactersList.Count; _index++)
            {
                if (_index != playerNumber)
                    _charactersList[_index].SetCharacterActive(false);
                else
                    _charactersList[_index].SetCharacterActive(true);
            }

            //Get all the sprite renderers in this player icon
            SpriteRenderer[] playerParts = _charactersList[playerNumber].GetAllSpriteRenderers();

            //If the player is unlocked, set this as the current player
            if (_charactersList[playerNumber].Unlocked)
            {
                IPHGameService.SelectedCharacter = playerNumber;

                //Go through all parts of the player and turn them opaque
                foreach (SpriteRenderer part in playerParts)
                    part.color = new Color(part.color.r, part.color.g, part.color.b, 1);

                //Deactivate the token icon
                _tokenObject?.SetActive(false);

            }
            else //Otherwise, display the number of tokens needed before this character is unlocked
            {
                //Go through all parts of the player and turn them transparent
                foreach (SpriteRenderer part in playerParts)
                    part.color = new Color(part.color.r, part.color.g, part.color.b, 0.3f);

                //Activate the token icon
                _tokenObject?.SetActive(true);

                //Display the number of tokens needed to unlock this player
                _tokenText.text = (_charactersList[playerNumber].TokensToUnlock - IPHDataStorage.playerData.tokens).ToString();
            }
        }
    }
}
