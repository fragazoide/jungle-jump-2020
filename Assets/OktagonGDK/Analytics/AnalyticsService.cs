﻿using System.Collections.Generic;
using UnityEngine;
using mixpanel;

public static class AnalyticsService
{
    const string FirstLaunchPrefsKey = "FirstLaunchPrefsKey";
    const string AccountUIDPrefsKey = "AccountUIDPrefsKey";
    private static string m_sAccountUID;

    #region Private Methods
    private static string GetAccountUID()
    {
        if (string.IsNullOrEmpty(m_sAccountUID))
        {
            if (!PlayerPrefs.HasKey(AccountUIDPrefsKey))
            {
                m_sAccountUID = System.Guid.NewGuid().ToString();
                PlayerPrefs.SetString(AccountUIDPrefsKey, m_sAccountUID);
            }
            else
            {
                m_sAccountUID = PlayerPrefs.GetString(AccountUIDPrefsKey);
            }
        }

        return m_sAccountUID;
    }

    private static bool IsFirstLaunch()
    {
        if (!PlayerPrefs.HasKey(FirstLaunchPrefsKey))
        {
            PlayerPrefs.SetString(AccountUIDPrefsKey, GetAccountUID());
            PlayerPrefs.SetString(FirstLaunchPrefsKey, SystemInfo.deviceUniqueIdentifier);
            return true;
        }
        else
        {
            return false;
        }
    }

    private static void TrackFirstLaunch()
    {
        var pProperties = new Value();
        pProperties["AccountId"] = GetAccountUID();
        pProperties["DeviceId"] = SystemInfo.deviceUniqueIdentifier;
        pProperties["DeviceModel"] = SystemInfo.deviceModel;
        pProperties["DeviceName"] = SystemInfo.deviceName;

        Mixpanel.Identify(GetAccountUID());
        Mixpanel.Track("1stLaunch", pProperties);
        LogEvent("1stLaunch", pProperties);
    }
    #endregion

    #region Public Methods
    
    public static void Initialize()
    {
        TrackEvent("Analytics Initialized");
        LogEvent("Analytics Initialized");
        if (IsFirstLaunch())
        {
            TrackFirstLaunch();
        }
    }

    /// <summary>
    /// Method to trigger events
    /// </summary>
    /// <param name="eventName"></param>
    /// <param name="properties"></param>
    public static void TrackEvent(string eventName, Value properties = null)
    {
        Mixpanel.Track(eventName, properties);
    }

    /// <summary>
    /// Method used only to test event triggering. (Use TrackEvent to really trigger events)
    /// </summary>
    /// <param name="sEventName"></param>
    /// <param name="pParams"></param>
    public static void LogEvent(string eventName, Value properties = null)
    {
        Debug.Log("Event: " + eventName + " - " + properties?.ToString());
    }

    #endregion
}
