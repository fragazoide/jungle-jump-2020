﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RarityType
{
    Common,
    Uncommon,
    Rare,
    Epic,
    Mythic,
    Legendary
}
