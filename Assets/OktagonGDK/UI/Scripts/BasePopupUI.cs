﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace OktagonGDK.UI
{
    public class BasePopupUI : MonoBehaviour
    {
        #region References
        [Header("Popup References")]
        [SerializeField] private GameObject _popupObject = default;
        [SerializeField] private RectTransform _popupTransform = default;
        #endregion

        #region Variables
        protected bool _isOpen;
        #endregion

        #region Public Methods
        public virtual void SetActive(bool active)
        {
            _popupObject.SetActive(active);
        }

        public virtual void Open(float duration = 1f, bool keepOpen = false)
        {
            StartCoroutine(OpenRoutine());

            IEnumerator OpenRoutine()
            {
                _isOpen = true;
                _popupObject.SetActive(true);
                _popupTransform.DOScale(1, 0.5f).SetEase(Ease.OutBack);

                if (!keepOpen)
                {
                    yield return new WaitForSeconds(duration);
                    Close();
                }
            }
        }

        public virtual void Close()
        {
            _popupTransform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                _isOpen = false;
                _popupObject.SetActive(false);
            });
        }
        #endregion
    }
}
