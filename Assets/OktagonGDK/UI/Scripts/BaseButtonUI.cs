﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace OktagonGDK.UI
{
    public class BaseButtonUI : Button
    {
        #region References
        [SerializeField] private List<Graphic> _additionalGraphics = default;
        #endregion

        #region Events
        public Action OnClickedEvent;
        public Action OnClickHoldEvent;
        public Action OnMouseDownEvent;
        public Action OnMouseUpEvent;
        #endregion

        #region Variables
        private bool _startClickTimer;
        private float _clickTimer;
        #endregion

        #region Private Methods
        private void Update()
        {
            if (_startClickTimer)
            {
                _clickTimer += Time.deltaTime;

                if (_clickTimer >= 1f)
                {
                    _startClickTimer = false;
                    _clickTimer = 0;
                    OnClickHoldEvent?.Invoke();
                }
            }
        }

        private void ExecuteColorTransition(Color color)
        {
            if (_additionalGraphics == null)
                return;

            foreach (var graphic in _additionalGraphics)
            {
                if (graphic != null)
                    graphic.CrossFadeColor(color, 0.15f, true, true);
            }
        }
        #endregion

        #region Public Methods
        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);

            if (!interactable)
                return;

            OnClickedEvent?.Invoke();
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            _startClickTimer = true;

            if (!interactable)
                return;

            OnMouseDownEvent?.Invoke();
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            _startClickTimer = false;
            _clickTimer = 0;

            if (!interactable)
                return;

            OnMouseUpEvent?.Invoke();
        }
        #endregion

        #region Protected Methods
        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            base.DoStateTransition(state, instant);

            switch (state)
            {
                case SelectionState.Normal:
                    ExecuteColorTransition(colors.normalColor);
                    break;
                case SelectionState.Highlighted:
                    ExecuteColorTransition(colors.highlightedColor);
                    break;
                case SelectionState.Pressed:
                    ExecuteColorTransition(colors.pressedColor);
                    break;
                case SelectionState.Selected:
                    ExecuteColorTransition(colors.selectedColor);
                    break;
                case SelectionState.Disabled:
                    ExecuteColorTransition(colors.disabledColor);
                    break;
            }
        }
        #endregion
    }
}
