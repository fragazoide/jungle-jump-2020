﻿using UnityEditor;
using UnityEditor.UI;

namespace OktagonGDK.UI
{
    [CustomEditor(typeof(BaseButtonUI))]
    public class BaseButtonEditor : ButtonEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.ApplyModifiedProperties();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("_additionalGraphics"), true);
            serializedObject.ApplyModifiedProperties();
        }
    }
}