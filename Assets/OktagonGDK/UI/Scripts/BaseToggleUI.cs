﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace OktagonGDK.UI
{
    public class BaseToggleUI : Toggle
    {
        #region Variables
        private bool _toggleState;
        private Color _toggleColor;
        #endregion

        #region Private Methods
        private void UpdateToggleState()
        {
            _toggleState = isOn;
        }

        private void UpdateToggleColor()
        {
            _toggleColor.a = _toggleState ? 1 : 0.5f;
            image.color = _toggleColor;
        }
        #endregion

        #region Protected Methods
        protected override void Awake()
        {
            base.Awake();
            _toggleState = isOn;
            _toggleColor = image.color;
        }
        #endregion

        #region Public Methods
        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
            UpdateToggleState();
            UpdateToggleColor();
        }

        public void SetState(bool state)
        {
            isOn = state;
            UpdateToggleState();
            UpdateToggleColor();
        }
        #endregion
    }
}