﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using OktagonGDK.UI;

public class RewardsUI : MonoBehaviour
{
    [SerializeField] private BaseButtonUI _rewardButton = default;
    [SerializeField] private RewardPopupUI _rewardPopup = default;
    [SerializeField] private RewardsProbabilityUI _rewardsProbability = default;

    public Action OnRewardClickedEvent;

    void Start()
    {
        _rewardButton.OnClickedEvent += OnRewardButtonClicked;
        _rewardButton.OnClickHoldEvent += OnRewardButtonHeld;
    }

    void Update()
    {
        
    }

    public void ConfigureProbabilityUI(string itemsProbabilities, string rarityProbabilities)
    {
        _rewardsProbability.ConfigureProbabilitiesText(itemsProbabilities, rarityProbabilities);
    }

    public void ConfigureRewardPopup(RewardItemData itemData)
    {
        _rewardPopup.ConfigurePopup(itemData.Name, itemData.Rarity.ToString(), itemData.RarityColor);
        _rewardPopup.Open();
    }

    public void SetRewardButtonInteractable(bool active)
    {
        _rewardButton.interactable = active;
    }

    private void OnRewardButtonClicked()
    {
        if (_rewardsProbability.IsShowing)
            return;

        OnRewardClickedEvent?.Invoke();
    }

    private void OnRewardButtonHeld()
    {
        _rewardsProbability.ShowPanel();
    }
}
