﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;
using InfiniteHopper;
using Random = UnityEngine.Random;

public class RewardsController : MonoBehaviour
{
    #region References
    [Header("References")]
    [SerializeField] private RewardsUI _rewardsUI = default;
    [SerializeField] private CountdownTimer _countdownTimer = default;

    [Header("Configuration")]
    [SerializeField] private string _csvFilename = default;
    [SerializeField] private List<Color> _rarityColors = default;
    #endregion

    #region Variables
    private List<RewardItemData> _allItems = new List<RewardItemData>();
    private Dictionary<RarityType, List<RewardItemData>> _itemsByRarity = new Dictionary<RarityType, List<RewardItemData>>();
    private bool _rewardIsAvailable;
    private float _totalItemsWeight;
    #endregion

    #region Private Methods
    private void Start()
    {
        _rewardsUI.OnRewardClickedEvent += OnSortNewReward;
        _countdownTimer.OnTimerCompleted += OnTimerCompleted;

        LoadTimerInformation();
        LoadRewardItemsFromCSV();
        ConfigureRewardItems();
        ConfigureRewardsUI();
    }

    private void LoadTimerInformation()
    {
        _rewardIsAvailable = true;
        _countdownTimer.SetActive(false);

        long rewardClaimTimerSaved = IPHDataStorage.playerData.rewardClaimTimer;

        if (rewardClaimTimerSaved != 0)
        {
            _rewardIsAvailable = false;
            _rewardsUI.SetRewardButtonInteractable(false);
            _countdownTimer.SetActive(true);
            _countdownTimer.LoadAndStartTimer(rewardClaimTimerSaved);
        }
    }

    private void LoadRewardItemsFromCSV()
    {
        List<List<string>> rewardsList = CSVLoader.ReadCSVFileFromResources(_csvFilename);

        foreach (var rewardLine in rewardsList)
        {
            RewardItemData chestItem = new RewardItemData()
            {
                Name = rewardLine[0],
                Weight = float.Parse(rewardLine[1], CultureInfo.InvariantCulture),
                Rarity = (RarityType)Enum.Parse(typeof(RarityType), rewardLine[2]),
            };

            _allItems.Add(chestItem);

            if (!_itemsByRarity.ContainsKey(chestItem.Rarity))
                _itemsByRarity.Add(chestItem.Rarity, new List<RewardItemData>() { chestItem });
            else
                _itemsByRarity[chestItem.Rarity].Add(chestItem);
        }
    }

    private void ConfigureRewardItems()
    {
         _totalItemsWeight = 0;

        foreach (var item in _allItems)
        {
            item.RarityColor = GetRarityColor(item.Rarity);
            item.SortRangeMin = _totalItemsWeight;
            item.SortRangeMax = _totalItemsWeight + item.Weight;
            _totalItemsWeight += item.Weight;
        }
    }

    private void ConfigureRewardsUI()
    {
        _rewardsUI.ConfigureProbabilityUI(GetProbabilitiesByItem(), GetProbabilitiesByRarity());
    }

    private Color GetRarityColor(RarityType rarity)
    {
        try
        {
            switch (rarity)
            {
                case RarityType.Common:
                    return _rarityColors[0];
                case RarityType.Uncommon:
                    return _rarityColors[1];
                case RarityType.Rare:
                    return _rarityColors[2];
                case RarityType.Mythic:
                    return _rarityColors[3];
            }
        }
        catch { return Color.black; }
        return Color.black;
    }

    private string GetProbabilitiesByRarity()
    {
        StringBuilder builder = new StringBuilder();

        foreach (var rarityTier in _itemsByRarity)
        {
            float tierWeight = 0;
            rarityTier.Value.ForEach(item => tierWeight += item.Weight);
            tierWeight = tierWeight / _totalItemsWeight;

            float tierOfferingPercentage = Mathf.Lerp(0, 100, tierWeight);
            builder.AppendLine(rarityTier.Key.ToString() + " : " + tierOfferingPercentage.ToString("0.00") + "%");
        }

        return builder.ToString();
    }

    private string GetProbabilitiesByItem()
    {
        StringBuilder builder = new StringBuilder();

        foreach (var item in _allItems)
        {
            float tierOfferingPercentage = Mathf.Lerp(0, 100, item.Weight / _totalItemsWeight);
            builder.AppendLine(item.Name.ToString() + " : " + tierOfferingPercentage.ToString("0.0000") + "%");
        }

        return builder.ToString();
    }

    private RewardItemData GetSortedItem()
    {
        float sortedNumber = Random.Range(1f, _totalItemsWeight);
        return _allItems.Find(item => sortedNumber >= item.SortRangeMin && sortedNumber < item.SortRangeMax);
    }
    #endregion

    #region Event Methods
    private void OnSortNewReward()
    {
        if (_rewardIsAvailable)
        {
            RewardItemData sortedItem = GetSortedItem();
            _rewardsUI.ConfigureRewardPopup(sortedItem);
            _rewardsUI.SetRewardButtonInteractable(false);
            
            _countdownTimer.SetActive(true);
            _countdownTimer.StartNewTimer();

            IPHDataStorage.playerData.rewardClaimTimer = _countdownTimer.CompletionTime;
        }
    }

    private void OnTimerCompleted()
    {
        _countdownTimer.SetActive(false);
        _rewardIsAvailable = true;
        _rewardsUI.SetRewardButtonInteractable(true);
        IPHDataStorage.playerData.rewardClaimTimer = 0;
    }
    #endregion
}
