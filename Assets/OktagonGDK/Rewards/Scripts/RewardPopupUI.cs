﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using OktagonGDK.UI;

public class RewardPopupUI : BasePopupUI
{
    [Header("Reward References")]
    [SerializeField] private TextMeshProUGUI _itemText = default;
    [SerializeField] private TextMeshProUGUI _rarityText = default;
    [SerializeField] private Button _claimButton = default;

    [Header("Animation References")]
    [SerializeField] private Image _faderImage = default;

    void Start()
    {
        _faderImage.DOFade(0f, 0f);
        _faderImage.gameObject.SetActive(false);
        _claimButton.onClick.AddListener(OnClaimButtonClicked);
    }

    public void ConfigurePopup(string itemName, string rarityName, Color rarityColor)
    {
        _itemText.text = itemName;
        _itemText.color = rarityColor;
        _rarityText.text = rarityName;
        _rarityText.color = rarityColor;
    }

    public override void Open(float duration = 1, bool keepOpen = true)
    {
        base.Open(duration, keepOpen);
        _faderImage.gameObject.SetActive(true);
        _faderImage.DOFade(0.5f, 0.5f);
    }

    public override void Close()
    {
        base.Close();
        StartCoroutine(CloseRoutine());

        IEnumerator CloseRoutine()
        {
            _faderImage.DOFade(0f, 0.5f);
            yield return new WaitForSeconds(0.5f);
            _faderImage.gameObject.SetActive(false);
        }
    }

    private void OnClaimButtonClicked()
    {
        Close();
    }
}
