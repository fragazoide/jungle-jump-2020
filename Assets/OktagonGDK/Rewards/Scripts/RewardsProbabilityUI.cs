﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class RewardsProbabilityUI : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private GameObject _panel = default;
    [SerializeField] private Transform _popup = default;
    [SerializeField] private Image _fader = default;
    [SerializeField] private Button _closeButton = default;

    [Header("Items References")]
    [SerializeField] private TextMeshProUGUI _itemsProbabilityText = default;
    [SerializeField] private GameObject _itemsProbabilityPanel = default;
    [SerializeField] private Button _itemsProbabilityButton = default;
    
    [Header("Rarity References")]
    [SerializeField] private TextMeshProUGUI _rarityProbabilityText = default;
    [SerializeField] private GameObject _rarityProbabilityPanel = default;
    [SerializeField] private Button _rarityProbabilityButton = default;

    public bool IsShowing { get; set; }

    private void Start()
    {
        _itemsProbabilityButton.onClick.AddListener(ShowItemsProbability);
        _rarityProbabilityButton.onClick.AddListener(ShowRarityProbability);
        _closeButton.onClick.AddListener(HidePanel);

        _popup.localScale = Vector3.zero;
        _fader.DOFade(0f, 0f);
    }

    public void ConfigureProbabilitiesText(string itemsProbability, string rarityProbability)
    {
        _itemsProbabilityText.text = itemsProbability;
        _rarityProbabilityText.text = rarityProbability;
    }

    public void ShowPanel()
    {
        _panel.gameObject.SetActive(true);
        _popup.DOScale(1, 0.5f).SetEase(Ease.OutBack);
        _fader.DOFade(0.5f, 0.5f);
        
        IsShowing = true;
        ShowRarityProbability();
    }

    public void HidePanel()
    {
        _popup.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() => _panel.gameObject.SetActive(false));
        _fader.DOFade(0, 0.5f);
        
        IsShowing = false;
    }

    private void ShowItemsProbability()
    {
        _itemsProbabilityPanel.SetActive(true);
        _rarityProbabilityPanel.SetActive(false);
    }

    private void ShowRarityProbability()
    {
        _itemsProbabilityPanel.SetActive(false);
        _rarityProbabilityPanel.SetActive(true);
    }
}
