﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardItemData
{
    public string Name;
    public float Weight;
    public RarityType Rarity;

    public Color RarityColor;
    public float SortRangeMin { get; set; }
    public float SortRangeMax { get; set; }
}
