﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using InfiniteHopper;

namespace OktagonGDK.Achievements
{
    public class AchievementController : SingletonBehaviour<AchievementController>
    {
        [Header("Achievements List")]
        [SerializeField] protected List<AchievementObject> _achievements = default;

        [Header("Achievements UI")]
        [SerializeField] protected AchievementPopupUI _achievementPopup = default;
        [SerializeField] protected AchievementsPanelUI _achievementPanel = default;

        private void Start()
        {
            Initialize();
            SceneManager.sceneLoaded += OnSceneLoaded;
            MainMenuController.Instance.OnAchievementsButtonClicked += OnActivatePanel;
        }

        protected virtual void Initialize()
        {
            foreach (var achievement in _achievements)
            {
                achievement.Initialize();
            }

            LoadAchivementsProgress();
        }

        protected virtual List<AchievementObject> GetAchievementByTrigger(string triggerName)
        {
            return _achievements.FindAll(achievement => achievement.TriggerName == triggerName);
        }

        public virtual void TriggerAchievement(AchievementTrigger trigger, string triggerParameter = "")
        {
            List<AchievementObject> achievements = GetAchievementByTrigger(trigger.Name);
            achievements.ForEach(achievement =>
            {
                if (triggerParameter == achievement.TriggerParameter)
                {
                    bool completedBefore = achievement.Completed;
                    achievement.IncreaseProgression();

                    if (achievement.Completed && !completedBefore)
                        _achievementPopup.ShowNotification(achievement);

                }
            });

            SaveAchievementsProgress();
        }

        private void SaveAchievementsProgress()
        {
            IPHDataStorage.playerData.achievementsProgression = _achievements.Select(achievement => achievement.GetProgressionData()).ToList();
        }

        private void LoadAchivementsProgress()
        {
            if (IPHDataStorage.playerData.achievementsProgression == null)
                return;

            List<AchievementProgression> progressionSaved = IPHDataStorage.playerData.achievementsProgression;

            foreach (var achievement in _achievements)
            {
                AchievementProgression progression = progressionSaved.Find(p => p.Name == achievement.Name);
                achievement.LoadProgression(progression);
            }
        }

        private void OnActivatePanel()
        {
            _achievementPanel.ConfigurePanel(_achievements);
            _achievementPanel.OpenPanel();
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode sceneMode)
        {
            if (scene.name == "CS_StartMenu")
                MainMenuController.Instance.OnAchievementsButtonClicked += OnActivatePanel;
        }
    }
}