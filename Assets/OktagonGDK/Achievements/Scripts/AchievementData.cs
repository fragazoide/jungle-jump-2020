﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OktagonGDK.Achievements
{
    [Serializable]
    public class AchievementData
    {
        [Header("Achievement Configuration")]
        public string Name = default;
        public string Description = default;
        public int Requirement = default;
        public Sprite Icon = default;

        [Header("Trigger Configuration")]
        public string TriggerName;
        public string TriggerParameter;
    }
}