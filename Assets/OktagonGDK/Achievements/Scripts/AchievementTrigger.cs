﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementTrigger : Enumeration
{
    public AchievementTrigger() { }

    public AchievementTrigger(int value, string name) : base(value, name) { }
}
