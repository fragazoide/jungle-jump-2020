﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OktagonGDK.Achievements
{
    [CreateAssetMenu(fileName = "Achievement", menuName = "Achievements/Create New Achievement", order = 1)]
    public class AchievementObject : ScriptableObject
    {
        [SerializeField] protected AchievementData _data = default;

        private AchievementProgression _progression;

        public string Name { get => _data.Name; }
        public string Description { get => _data.Description; }
        public Sprite Icon { get => _data.Icon; }
        public int Requirement { get => _data.Requirement; }

        public string TriggerName { get => _data.TriggerName; }
        public string TriggerParameter { get => _data.TriggerParameter; }

        public int Progress { get => _progression.Progress; }
        public bool Completed { get => _progression.Completed; }

        public virtual void Initialize()
        {
            _progression = new AchievementProgression(_data.Name);
        }

        public virtual void LoadProgression(AchievementProgression progression)
        {
            if (progression != null)
                _progression = progression;
        }

        public virtual void IncreaseProgression()
        {
            if (!_progression.Completed)
            {
                _progression.Progress += 1;
                _progression.Completed = _progression.Progress >= _data.Requirement;
            }
        }

        public virtual AchievementProgression GetProgressionData()
        {
            return _progression;
        }
    }
}