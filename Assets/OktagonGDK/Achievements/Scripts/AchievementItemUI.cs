﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace OktagonGDK.Achievements
{
    public class AchievementItemUI : MonoBehaviour
    {
        [Header("Achievement References")]
        [SerializeField] private TextMeshProUGUI _description = default;
        [SerializeField] private TextMeshProUGUI _progression = default;
        [SerializeField] private Image _progressionFill = default;
        [SerializeField] private Image _icon = default;
        [Space]
        [SerializeField] private Color _completedColor = default;
        [SerializeField] private Color _progressColor = default;

        public void ConfigureItem(AchievementObject achievement)
        {
            _description.text = achievement.Description;
            _icon.sprite = achievement.Icon;

            if (achievement.Completed)
            {
                _progression.text = "Completed";
                _progression.color = Color.white;
                _progressionFill.fillAmount = 1;
                _progressionFill.color = _completedColor;
            }
            else
            {
                _progression.text = string.Format("{0} / {1}", achievement.Progress, achievement.Requirement);
                _progressionFill.fillAmount = ((float)achievement.Progress / (float)achievement.Requirement);
                _progressionFill.color = _progressColor;
            }
        }
    }
}