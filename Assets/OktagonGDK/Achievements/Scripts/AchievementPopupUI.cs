﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using OktagonGDK.UI;
using System.Linq;

namespace OktagonGDK.Achievements
{
    public class AchievementPopupUI : BasePopupUI
    {
        [Header("Achievement References")]
        [SerializeField] private TextMeshProUGUI _description = default;
        [SerializeField] private TextMeshProUGUI _progression = default;
        [SerializeField] private Image _progressionFill = default;
        [SerializeField] private Image _icon = default;

        private List<AchievementObject> _notificationQueue = new List<AchievementObject>();

        void Start()
        {

        }

        public void ShowNotification(AchievementObject achievement)
        {
            _notificationQueue.Add(achievement);

            if (!_isOpen)
            {
                ConfigureNotification(achievement);
                Open();
            }
        }

        public override void Close()
        {
            base.Close();

            _notificationQueue.Remove(_notificationQueue.First());
            ShowNextNotification();
        }

        private void ConfigureNotification(AchievementObject achievement)
        {
            _description.text = achievement.Description;
            _icon.sprite = achievement.Icon;

            if (achievement.Completed)
            {
                _progression.text = "Completed";
                _progressionFill.fillAmount = 1;
                _progressionFill.color = Color.green;
            }
        }

        private void ShowNextNotification()
        {
            StartCoroutine(QueueRoutine());

            IEnumerator QueueRoutine()
            {
                yield return new WaitForSeconds(0.55f);

                if (_notificationQueue.Count > 0)
                {
                    ConfigureNotification(_notificationQueue.First());
                    Open();
                }
            }
        }
    }
}