﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace OktagonGDK.Achievements
{
    public class AchievementsPanelUI : MonoBehaviour
    {
        [Header("Panel")]
        [SerializeField] private GameObject _pivot = default;
        [SerializeField] private Transform _panel = default;
        [SerializeField] private Image _fader = default;
        [SerializeField] private Button _closeButton = default;

        [Header("Achievements")]
        [SerializeField] private AchievementItemUI _achievementItemPrefab = default;
        [SerializeField] private Transform _achievementsContainer = default;

        private List<AchievementItemUI> _achievementItems = new List<AchievementItemUI>();

        private void Start()
        {
            _fader.DOFade(0, 0);
            _panel.localScale = Vector3.zero;
            _closeButton.onClick.AddListener(ClosePanel);
        }

        public void ConfigurePanel(List<AchievementObject> achievements)
        {
            if (_achievementItems == null || _achievementItems.Count == 0)
                CreateItems(achievements);
            else
                UpdateItems(achievements);
        }

        private void CreateItems(List<AchievementObject> achievements)
        {
            foreach (var achievement in achievements)
            {
                AchievementItemUI item = Instantiate(_achievementItemPrefab, _achievementsContainer);
                item.ConfigureItem(achievement);
                _achievementItems.Add(item);
            }
        }

        private void UpdateItems(List<AchievementObject> achievements)
        {
            for (int i = 0; i < achievements.Count; i++)
            {
                _achievementItems[i].ConfigureItem(achievements[i]);
            }
        }

        public void OpenPanel()
        {
            _pivot.SetActive(true);
            _fader.DOFade(0.5f, 0.5f);
            _panel.DOScale(1, 0.5f).SetEase(Ease.OutBack);
        }

        public void ClosePanel()
        {
            _fader.DOFade(0f, 0.5f);
            _panel.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() => _pivot.SetActive(false));
        }
    }
}