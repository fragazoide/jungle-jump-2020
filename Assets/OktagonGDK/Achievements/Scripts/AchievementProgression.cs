﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OktagonGDK.Achievements
{
    [Serializable]
    public class AchievementProgression
    {
        public string Name;
        public int Progress;
        public bool Completed;

        public AchievementProgression()
        {
            Progress = 0;
            Completed = false;
        }

        public AchievementProgression(string achievementName)
        {
            Progress = 0;
            Completed = false;
            Name = achievementName;
        }
    }
}