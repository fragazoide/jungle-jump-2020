﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour
{
    #region References
    [Header("References")]
    [SerializeField] private GameObject _pivot = default;
    [SerializeField] private Text _timerText = default;
    
    [Header("Configuration")]
    [SerializeField] private int _hours = default;
    [SerializeField] private int _minutes = default;
    [SerializeField] private int _seconds = default;
    #endregion

    #region Events
    public Action OnTimerStarted;
    public Action OnTimerCompleted;
    #endregion

    #region Properties
    public long CompletionTime { get => _completionTime; }
    #endregion

    #region Variables
    private bool _startTimer;
    private long _completionTime;
    private TimeSpan _currentTime;
    #endregion

    #region Private Methods
    private void Update()
    {
        if (_startTimer)
        {
            _currentTime = new TimeSpan(_completionTime - DateTime.Now.Ticks);
            UpdateTimerText();

            if (_currentTime.TotalSeconds <= 0)
                CompleteTimer();
        }
    }

    private void UpdateTimerText()
    {
        if (_timerText)
            _timerText.text = string.Format("{0}:{1}:{2}", _currentTime.Hours.ToString("00"), _currentTime.Minutes.ToString("00"), _currentTime.Seconds.ToString("00"));
    }
    #endregion

    #region Public Methods
    public void SetActive(bool active)
    {
        _pivot.SetActive(active);
    }

    public void StartNewTimer()
    {
        _startTimer = true;
        _completionTime = DateTime.Now.AddHours(_hours).AddMinutes(_minutes).AddSeconds(_seconds).Ticks;
        OnTimerStarted?.Invoke();
    }

    public void CompleteTimer()
    {
        _startTimer = false;
        _completionTime = 0;
        OnTimerCompleted?.Invoke();
    }

    public void LoadAndStartTimer(long completionTime)
    {
        _startTimer = true;
        _completionTime = completionTime;
        OnTimerStarted?.Invoke();
    }
    #endregion
}
