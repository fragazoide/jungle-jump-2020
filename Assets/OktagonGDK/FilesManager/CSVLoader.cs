﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public static class CSVLoader
{
    public static List<List<string>> ReadCSVFileFromPath(string filePath)
    {
        string[] file = File.ReadAllText(filePath).Split('\n');
        return ConvertCSVToList(file);
    }

    public static List<List<string>> ReadCSVFileFromResources(string resourcePath)
    {
        TextAsset textAsset = Resources.Load<TextAsset>(resourcePath);
        string[] file = textAsset.text.Split('\n');
        return ConvertCSVToList(file);
    }

    private static List<List<string>> ConvertCSVToList(string[] csvText)
    {
        List<List<string>> textConverted = new List<List<string>>();

        for (int lineIndex = 0; lineIndex < csvText.Length; lineIndex++)
        {
            if (csvText[lineIndex].StartsWith("#"))
                continue;

            string[] line = csvText[lineIndex].Split(',');
            textConverted.Add(new List<string>());

            for (int wordIndex = 0; wordIndex < line.Length; wordIndex++)
            {
                textConverted.Last().Add(line[wordIndex]);
            }
        }

        return textConverted;
    }
}
